import React, { Component } from 'react';
import ProyectosServicio from '../../servicios/proyectos.servicio';
import CiudadesServicio from '../../servicios/ciudades.servicio'
import NombresProyectos from '../../servicios/nombres-proyectos.servicio'

import { connect } from 'react-redux'
import * as usuariosActions from '../../actions/usuariosActions'

import './estilos.css'


import { ValidarFormulario } from '../comunes/validacionFormularios'

class FormularioComponente extends Component {
    constructor(props) {
        super(props)
        this.save = this.save.bind(this)
        this.masImagenes = this.masImagenes.bind(this)

        this.state = {
            ciudades: [],
            tipoInmueble: [],
            descripcion: [],
            imagenes: [
                {
                    id: 1,
                    url: ''
                }
            ]
        }
    }

    async componentDidMount() {
        let ciudadesServicio = new CiudadesServicio()
        let ciudades = await ciudadesServicio.traerCiudades()

        let proyectosServicio = new ProyectosServicio()
        let tipoInmueble = await proyectosServicio.getProyectos()

        let nombresProyectos = new NombresProyectos()
        let descripcion = await nombresProyectos.getNombresProyectos()

        this.setState({
            ciudades: ciudades,
            tipoInmueble: tipoInmueble,
            descripcion: descripcion
        })

        const validadorFormulario = new ValidarFormulario()
        validadorFormulario.validar()
    }

    // async traerDatosTipoInmueble(){
    //     let proyectosServicio = new ProyectosServicio()
    //     let inmueble = await proyectosServicio.getProyectos()
    //     this.setState({
    //         inmueble: inmueble
    //     })
    // }

    // async traerNomresProyectos(){
    //     let nombresProyectos = new NombresProyectos()
    //     let descripcion = await nombresProyectos.getNombresProyectos()
    //     this.setState({
    //         descripcion: descripcion
    //     })
    // }


    render() {
        return (
            <div>
                {
                    !this.props.usuario &&
                    <form className="container contenedor">

                        <p style={{
                            fontSize: '1.35em',
                            margin: 10,
                            color: '#F3A02B'
                        }}>Crea un nuevo proyecto</p>

                        <div className="">
                            <div className="row">
                                <div className="col input-solos">
                                    <label>Nombre del proyecto</label>
                                    <input className="input" id="nombre"
                                        name="nombre" />

                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <label>Ciudad </label>
                                    <select className="ciudad form-control" name="ciudad"
                                        required>
                                        {this.state.ciudades.map(item =>
                                            <option key={item.data.id}>{item.data.nombre}</option>
                                        )}
                                    </select>
                                </div>
                                <div className="col">
                                    <label>Tipo de Inmueble </label>
                                    <select className="tipoInmueble form-control" name="tipoInmueble"
                                        required>
                                        {this.state.tipoInmueble.map(item =>
                                            <option key={item.data.id}>{item.data.tipoInmueble}</option>
                                        )}
                                    </select>
                                </div>
                                <div className="col">
                                    <label>Tipo </label>
                                    <select className="tipo form-control" name="tipo"
                                        required>
                                        {this.state.descripcion.map(item =>
                                            <option key={item.data.id}>{item.data.nombre}</option>
                                        )}</select>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col input-solos">
                                    <label>Descripcion</label>
                                    <input className="descripcion" name="descripcion"
                                        required />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col input-solos">
                                    <label>Agregar imagenes <a onClick={this.masImagenes}
                                        style={{ paddingLeft: 15, fontSize: '1.25em', cursor: 'pointer' }}>
                                        <i className="fa fa-plus"></i>
                                    </a></label>
                                    {this.state.imagenes.map(imagen =>
                                        <div key={imagen.id}>
                                            <input className="imagen" placeholder={'Pega aqui el link de tu imagen ' + imagen.id}
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div>
                                <button type="submit" className="btn btn-primary" style={{ margin: 10 }}
                                    onClick={this.save}>Guardar</button>
                            </div>

                        </div>
                    </form>
                }
                <div className="spinner-border text-warning" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        )
    }

    save(event) {
        const $nombre = document.getElementById('nombre')
        const $ciudad = document.getElementsByClassName("ciudad")[0]
        const $tipoInmueble = document.getElementsByClassName("tipoInmueble")[0]
        const $tipo = document.getElementsByClassName("tipo")[0]
        const $descripcion = document.getElementsByClassName("descripcion")[0]

        let valid = this.validator(
            $nombre.value,
            $ciudad.value,
            $tipoInmueble.value,
            $tipo.value,
            $descripcion.value
        )

        if (valid) {
            let servicioGuardar = new NombresProyectos()
            servicioGuardar.saveNombreProyecto(
                $nombre.value.trim(),
                $ciudad.value.trim(),
                $tipoInmueble.value.trim(),
                $descripcion.value.trim()
            )
                .then((res) => {
                    console.log(res)
                })
                .catch((e) => {
                    console.log('error en guardado')
                })
            console.log('guardo bien')
        } else {
            console.log('error')
        }
    }

    validator(nombre, ciudad, tipoInmueble, tipo, descripcion) {
        let respuesta = false
        if (nombre && ciudad && tipoInmueble && tipo && descripcion) {
            descripcion.length > 300 ? respuesta = false : respuesta = true
        }
        return respuesta
    }

    masImagenes() {
        console.log('entro')
        const longitud = this.state.imagenes.length
        this.setState(prevState => ({
            imagenes: [...prevState.imagenes, {
                id: longitud + 1,
                url: ''
            }]
        }))
    }
}

const mapStateToProps = (reducers) => reducers.usuarioReduce

export default connect(mapStateToProps, usuariosActions)(FormularioComponente);