export class ValidarFormulario {

    FORMULARIO = {}

    constructor() {
        // this.FORMULARIO = document.getElementById(formulario)
    }

    validar() {
        let msg = "";
        let elements = document.getElementsByTagName("INPUT")
        for (let i = 0; i <= elements.length; i++) {
            i = parseInt(i)
            if (i) {
                elements[i - 1].oninvalid
                    = function (e) {
                        console.log(e.target.validity.valid)
                        if (!e.target.validity.valid) {
                            switch (e.target.name) {
                                case 'nombre':
                                    e.target.setCustomValidity("El campo nombre es requerido"); break;
                                case 'descripcion':
                                    e.target.setCustomValidity("el campo descripción es requerido"); break;
                                default: e.target.setCustomValidity(""); break;

                            }
                        }
                    };
                elements[i - 1].oninput = function (e) {
                    e.target.setCustomValidity(msg);
                };
            }
        }
    }

}