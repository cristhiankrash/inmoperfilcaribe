import React, { Component } from 'react';
import { AutenticacionServicio } from '../../servicios/autenticacion.servicio';
import { ToastsContainer, ToastsStore } from 'react-toasts';
import './registrar.css';
import * as usuariosActions from '../../actions/usuariosActions'
import { connect } from 'react-redux'

class RegistrarUsuarioComponente extends Component {

    constructor(props) {
        super(props)
        this.registrarUsuario = this.registrarUsuario.bind(this)
        this.state = {
            permiso: true
        }
    }

    componentDidMount() {        
        this.props.usuario ? this.setState({ permiso: true }) : this.setState({ permiso: false })
    }

    render() {
        return (
            <div className="container register-form">
                {this.state.permiso &&
                    <div className="form">
                        <div className="note">
                            <p>Crea un nuevo usuario administrador</p>
                        </div>
                        <div id="registrar-usuario" className="form-content">
                            <div className="row">
                                <div className="col-sm-12 col-md-6">
                                    <div className="form-group">
                                        <input id="nombre" className="form-control" name="nombre" type="text" placeholder="Nombre"></input>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <div className="form-group">
                                        <input id="correo" className="form-control" name="correo" type="email" placeholder="Correo electrónico"></input>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12 col-md-6">
                                    <div className="form-group">
                                        <input id="pass" className="form-control" name="pass" type="password" placeholder="Contraseña"></input>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <div className="form-group">
                                        <input id="pass2" className="form-control" name="pass2" type="password" placeholder="Validar contraseña"></input>
                                    </div>
                                </div>
                            </div>
                            <button type="button" className="btn btn-success" onClick={this.registrarUsuario}>Crear usuario</button>
                            <ToastsContainer store={ToastsStore} />
                        </div>
                    </div>
                }


            </div>
        )
    }

    async registrarUsuario() {
        let $nombre = document.getElementById('nombre').value
        let $correo = document.getElementById('correo').value
        let $pass = document.getElementById('pass').value
        let $pass2 = document.getElementById('pass2').value
        let validador = this.validarCampos($nombre, $correo, $pass, $pass2)
        if (!validador.estado) {
            ToastsStore.error(`${validador.mensaje}`)
            return
        }
        let autenticacionServicio = new AutenticacionServicio()
        let respuesta = await autenticacionServicio.crearUsuario($correo, $pass, $nombre)
        respuesta.estado ? ToastsStore.success('Se ha agregado un nuevo usuario') :
            ToastsStore.success('Ha ocurrido un error, intentalo mas tarde')
    }

    validarCampos(nombre = '', correo = '', pass = '', pass2 = '') {
        if (nombre.trim() && correo.trim() && pass.trim() && pass2.trim()) {
            if (pass === pass2) return { estado: true }
            return { estado: false, mensaje: 'La contraseña no cohincide' }
        }
        return { estado: false, mensaje: 'Debes completar todos los campos requeridos (*)' }
    }

}

const mapStateToProps = (reducers) => {
    return reducers.usuarioReduce
}

export default connect(mapStateToProps, usuariosActions)(RegistrarUsuarioComponente);