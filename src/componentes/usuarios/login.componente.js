import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { ToastsContainer, ToastsStore } from 'react-toasts';

import './login.css';
import logo from '../../img/Logo_IPC.svg';

import { AutenticacionServicio } from '../../servicios/autenticacion.servicio';


class LoginComponente extends Component {

    constructor(props) {
        super(props)
        this.state = {
            usuarioActivo: false
        }
        this.loguearse = this.loguearse.bind(this)
        this.redireccionar = this.redireccionar.bind(this)
    }

    render() {
        return (
            <div>                
                <div className="wrapper fadeInDown">
                    <div id="formContent">

                        <div className="fadeIn first">
                            <img src={logo} height="100px" width="100px" id="icon" alt="User Icon" />
                        </div>

                        <form>
                            <input type="email" id="login" className="fadeIn second" name="login" 
                            autoComplete="off" title="Ingrese un correo valido" placeholder="Correo eléctronico" required/>
                            <input type="password" id="password" className="fadeIn third" name="login"
                            autoComplete="off" title="Ingrese la contraseña" placeholder="Contraseña" required/>
                            <input type="submit" onClick={this.loguearse} className="fadeIn fourth" value="Log In" />
                        </form>

                        <div id="formFooter">
                            <a className="underlineHover" href="#">Olvidaste tu contraseña?</a>
                            {this.redireccionar()}
                        </div>

                    </div>
                    <ToastsContainer store={ToastsStore} />
                </div>
            </div>
        )
    }

    async loguearse(event) {
        event.preventDefault();
        let $inputs = document.querySelectorAll('input')
        let correo = $inputs[0].value
        let contraseña = $inputs[1].value
        let autenticacionServicio = new AutenticacionServicio()
        let respuesta = await autenticacionServicio.login(correo, contraseña)
        if (respuesta.user) {
            this.setState({ usuarioActivo: true })
            this.props.loguearse(true)
            return true
        }
        let mensaje = this.posiblesrespuestas(respuesta.code)
        ToastsStore.error(mensaje)
        this.props.loguearse(false)
        return false
    }

    redireccionar(){
        if(this.state.usuarioActivo) return <Redirect to='/' />
    }

    posiblesrespuestas(codigo) {
        if (codigo === "auth/user-not-found") return "Correo electrónico no encontrado"
        if (codigo === "auth/wrong-password") return "Contraseña incorrecta"
        return "Verifique los campos"
    }

}

export default LoginComponente;