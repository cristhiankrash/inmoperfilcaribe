import React, { Component } from 'react'

import CiudadesServicio from '../../servicios/ciudades.servicio'
import ProyectosServicio from '../../servicios/proyectos.servicio'
import NombreProyectosServicio from '../../servicios/nombres-proyectos.servicio'

import './inicio.component.css'
import imagenBanner from '../../img/header-bg.jpg'

class InicioComponente extends Component {
    constructor() {
        super()
        this.state = {
            tiposInmuebles: []
        }
    }

    async componentDidMount() {
        // this.traerCiudadeId()
        // this.traerCiudades()
        // this.getProyectos()
        // this.getNombreProyecto()
        const proyectosServicio = new ProyectosServicio()
        const tipoInmueble = await proyectosServicio.getProyectos()
        this.setState({ tiposInmuebles: tipoInmueble })

        const hamburger = document.querySelector(".hamburguer");
        console.log(hamburger)
        const navLinks = document.querySelector(".nav-links");
        const links = document.querySelectorAll(".nav-links li");

        hamburger.addEventListener("click", () => {
            navLinks.classList.toggle("open");
            links.forEach(link => {
                link.classList.toggle("fade");
            });
        });
    }

    render() {
        const estiloLinea = {
            cardHorizontal: {
                display: 'flex'
            }
        }
        return (
            //recuerda que cuadno quieras listar un elemento puedes hacer uso del .map()...
            <div>
                <div className="imagen-encabezado">
                    <img src={imagenBanner} alt="Inmobiliaria Perfil Caribe" />
                    <div className="elementos-busqueda-ipc">
                        <p className="text-titulo">
                            <span>Inmobiliaria</span> Perfil Caribe
                        </p>
                        <div className="form-inline">
                            <select className="form-control mr-3" placeholder="¿Que buscas?">
                                <option value="">¿Que estas buscando?</option>
                                {this.state.tiposInmuebles.map(inmueble =>
                                    <option key={inmueble.data.id}>{inmueble.data.tipoInmueble}</option>
                                )}
                            </select>
                            <button className="btn btn-primary ml-0"> Buscar</button>
                        </div>
                    </div>
                </div>
                <div className="container" style={{ paddingTop: 40 }}>
                    <div className="card card-kr" style={{ width: '18rem' }}>
                        <img className="card-img-top" src={"https://i.blogs.es/8e8f64/lo-de-que-comprar-una-casa-es-la-mejor-inversion-hay-generaciones-que-ya-no-lo-ven-ni-de-lejos---1/450_1000.jpg"}
                            alt="Card image cap" />
                        <div className="card-body">
                            <h5 className="card-title">Card title</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" className="btn btn-primary">Ver</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    async traerCiudadeId() {
        let ciudadesServicio = new CiudadesServicio()
        let ciudades = await ciudadesServicio.traerCiudadePorId('V9vQmLpLeAmd611Y4sRD')
    }

    async traerCiudades() {
        let ciudadesServicio = new CiudadesServicio()
        let ciudades = await ciudadesServicio.traerCiudades()
    }

    async getProyectos() {
        const proyectosServicio = new ProyectosServicio()
        const tipoInmueble = await proyectosServicio.getProyectos()
        if (tipoInmueble.estado) return tipoInmueble
    }

    async getNombreProyecto() {
        let nombreProyectoServicio = new NombreProyectosServicio()
        let nombre = await nombreProyectoServicio.getNombresProyectos()
    }



}

export default InicioComponente;