import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import * as usuariosActions from '../actions/usuariosActions'
import './encabezado.css'
import Logo from '../img/Logo_IPC.png'

import { AutenticacionServicio } from '../servicios/autenticacion.servicio';

class EncabezadoComponente extends Component {
    constructor(props) {
        super(props)
        this.cerrarSesion = this.cerrarSesion.bind(this)
    }

    render() {
        return (
            <header>
                <nav>
                    <div className="nav-ipc px-5">
                        <div className="box-logo-ipc" >
                            <Link to='/' >
                                <img className="logo-ipc" src={Logo} />
                            </Link>
                        </div>
                        <div className="box-items-ipc">
                            <div className="hamburguer">
                                <div className="line"></div>
                                <div className="line"></div>
                                <div className="line"></div>
                            </div>
                            <div className="nav-links">
                                <li className="">
                                    <Link to='/busqueda' className="nav-link">Categoría</Link>
                                </li>
                                <li className="">
                                    <Link to='/contactanos' className="nav-link" >Contactanos</Link>
                                </li>
                                <li className="">
                                    <Link to='/nuevo' className="nav-link">Nuevo Proyecto</Link>
                                </li>
                                {this.props.usuarioActivo &&
                                    <li className="">
                                        <Link to='/agregar' className="nav-link">Agregar Proyecto</Link>
                                    </li>
                                }
                                {this.props.usuarioActivo &&
                                    <li className="">
                                        <Link to="/nuevo/usuario" className="nav-link">Agregar usuario</Link>
                                    </li>
                                }
                                <li className="">
                                    {
                                        this.props.usuarioActivo ?
                                            <Link className="" onClick={this.cerrarSesion}
                                                to='/'>Cerrar sesión</Link>
                                            : <Link to="/login" className="nav-link">Iniciar sesión</Link>
                                    }
                                </li>
                            </div>
                        </div>
                    </div>
                </nav>
                {/* <nav className="encabezado-principal navbar fixed-top navbar-expand-lg scrolling-navbar">
                    <Link to='/' className="navbar-brand">
                        <img src={Logo} height="25%" width="25%" />
                    </Link>
                    
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link to='/busqueda' className="nav-link">Categoría</Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/contactanos' className="nav-link" >Contactanos</Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/nuevo' className="nav-link">Nuevo Proyecto</Link>
                            </li>
                            {this.props.usuarioActivo &&
                                <li className="nav-item">
                                    <Link to='/agregar' className="nav-link">Agregar Proyecto</Link>
                                </li>
                            }
                            {this.props.usuarioActivo &&
                                <li className="nav-item">
                                    <Link to="/nuevo/usuario" className="nav-link">Agregar usuario</Link>
                                </li>
                            }
                            <li className="nav-item">
                                {
                                    this.props.usuarioActivo ?
                                        <Link className="nav-link" onClick={this.cerrarSesion}
                                            to='/'>Cerrar sesión</Link>
                                        : <Link to="/login" className="nav-link">Iniciar sesión</Link>
                                }
                            </li>
                        </ul>
                    </div>
                </nav> */}

            </header>
        )
    }

    async cerrarSesion() {
        let autenticacionServicio = new AutenticacionServicio()
        await autenticacionServicio.cerrarSesion()
        this.props.desloguearse(false)
    }

}

const mapStateToProps = (reducers) => reducers.usuarioReduce

export default connect(mapStateToProps, usuariosActions)(EncabezadoComponente)