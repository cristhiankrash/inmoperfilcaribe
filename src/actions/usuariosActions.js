import { AutenticacionServicio } from '../servicios/autenticacion.servicio';
import { VALIDAR_USUARIO, CARGANDO } from '../tipos'

export const verificarUsuario = () => (dispatch) => {
    // dispatch({
    //     type: ERROR
    // })
    dispatch({
        type: CARGANDO
    })
    const autenticacionServicio = new AutenticacionServicio()
    const usuario = autenticacionServicio.validarUsuario()
    dispatch({
        type: VALIDAR_USUARIO,
        payload: usuario
    })
}

