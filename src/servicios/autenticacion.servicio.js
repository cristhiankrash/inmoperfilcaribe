import firebase from '../firestore';

export class AutenticacionServicio {

    login(correo, contrasena) {
        return firebase.auth().signInWithEmailAndPassword(correo, contrasena)
            .then(response => {
                console.log(response)
                return response
            })
            .catch(error => {
                console.log(error)
                return error
            })
    }

    cerrarSesion() {
        return firebase.auth().signOut()
            .then(respuesta => {
                let res = {
                    estado: true, data: respuesta
                }
                return res
            })
            .catch(error => {
                let err = {
                    estado: false, error: error
                }
                return err
            })
    }

    validarUsuario(){
        let usuario = firebase.auth().currentUser
        return usuario
    }

    crearUsuario(correo, contrasena, nombre) {
        return firebase.auth().createUserWithEmailAndPassword(correo, contrasena)
            .then(resultado => {
                console.log("resultado")
                console.log(resultado)
                resultado.user.updateProfile({
                    displayName: nombre
                })

                const configuracion = {
                    url: 'http://localhost:3000/'
                }

                resultado.user.sendEmailVerification(configuracion)
                    .catch(error => {
                        console.log("error al enviar correo de confirmación")
                        console.log(error)
                        return { estado: false }
                    })

                firebase.auth().signOut()
                return { estado: true, data: resultado.user.uid }
            })
            .catch(error => {
                console.log("error en crear usuario");
                console.log(error)
                return { estado: false }
            })
    }
}