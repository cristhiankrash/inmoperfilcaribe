import firebase from '../firestore';

export default class NombresProyectos {

    db = firebase.firestore()
    constructor() { }

    saveNombreProyecto = (nombre, ciudadId, tipoId, descripcion) => {
        return this.db.collection("nombre-proyecto").add({
            nombre: nombre,
            ciudadId: ciudadId, // ojo que ya vamos aguardar un objeto no un strig
            tipoId: tipoId, // ojo que ya vamos aguardar un objeto no un strig
            descripcion: descripcion
        })
    }

    // get proyecto
    getNombresProyectos = () => {
        let nombre = {}
        let descripcion = []
        return this.db.collection('nombre-proyecto').get()
            .then(res => {
                res.forEach(item => {
                    let data = {
                        id: item.id,
                        ...item.data()
                    }
                    nombre = { estado: true, data: data }
                    descripcion.push(nombre)
                })
                return descripcion
            })
            .catch(error => {
                return { estado: true, error: error }
            })
    }
}