import firebase from '../firestore';

export default class CiudadesServicio {

    db = firebase.firestore()
    constructor() { }

    /***
     * guarda ciudad la funcion recibe el nombre de la ciudad y el departamento
     * si agrego add firestore me crea el id automatico si no quiero id automatico
     * 
     */
    guardarCiudad = (nombre, departamento) => {
        return this.db.collection("ciudades").add({
            nombre: nombre,
            departamento: departamento
        })
    }

    /***
     * Queda suscrito a la colexión post
    ***/
    // consultarWebSocket(){
    //     return this.db.collection('ciudades').onSnapshot(querySnapshot => {

    //     })
    // }

    /***
     * trae todas la ciudades recorre la respuesta y a cada una la agrega con push a otro array
     * asi se devuelve solo un array con todos los objetos
     */
    traerCiudades = () => {
        let ciudades = []
        let ciudad = {}
        return this.db.collection('ciudades').get()
            .then(respuesta => {
                respuesta.forEach(item => {
                    let data = {
                        id: item.id,
                        ...item.data()
                    }
                    ciudad = { estado: true, data: data }
                    ciudades.push(ciudad)
                })
                return ciudades
            })
            .catch(error => {
                return { estado: false, error: error }
            })
    }

    /***
     * trae una ciudad por su id
     */
    traerCiudadePorId = (ciudadID) => {
        return this.db.collection('ciudades').doc(ciudadID).get()
            .then(respuesta => {
                let data = {
                    id: respuesta.id,
                    ...respuesta.data()
                }
                return { estado: true, data: data }
            })
            .catch(error => {
                return { estado: false, error: error }
            })
    }

    // usando where
    // this.db.collection('ciudades') 
    // .where('autor','==',email)

    // subirImagenes (file,proyecto) {
    //     const referenciaStorage = firebase.storage().ref(`img/${proyecto}/${file.name}`)
    //     const tarea = referenciaStorage.storage.put(file)
    //     //leer los eventos que hace un metodo asyncrono
    //     tarea.on('state_changed',
    //         snapshot => {
    //             //Obtengo porcentaje 
    //             const porcentaje = snapshot.bytesTransferred / snapshot.totalBytes * 100
    //         },
    //         error => {
    //             console.log('error subien do el archivo')
    //         },
    //         completada => {
    //             tarea.snapshot.ref.getDownloadURL()
    //                 .then(url => {
    //                     console.log(`obtener url de descarga ${url}`)
    //                     sessionStorage.setItem('imgNueva',url)
    //                 })
    //                 .catch(error => console.log("error objteniendo url de descarga"))
    //         }
    //     )
    // }

}