import firebase from '../firestore';

export default class ProyectosServicio {

    db = firebase.firestore()
    constructor() { }

    saveTipoInmueble = (proy, inmueble) => {
        return this.db.collection("proyectos").add({
            proy: proy,
            inmueble: inmueble
        })
    }

    // get proyecto
    getProyectos = () => {
        let proyectos = {}
        let tipoInmueble = []
        return this.db.collection('proyectos').get()
            .then(res => {
                res.forEach(item => {
                    let data = {
                        id: item.id,
                        ...item.data()
                    }
                    proyectos = { estado: true, data: data }
                    tipoInmueble.push(proyectos)
                })
                return tipoInmueble
            })
            .catch(error => {
                return { estado: true, error: error }
            })
    }
}