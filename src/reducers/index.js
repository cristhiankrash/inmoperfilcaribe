import { combineReducers } from 'redux'
import usuarioReduce from './usuarioReduce'

export default combineReducers({
    usuarioReduce,
});