import { VALIDAR_USUARIO, CARGANDO } from '../tipos'

const INITIAL_STATE = {
    usuario: '',
    cargando: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case VALIDAR_USUARIO:
            return { ...state,
                usuario: action.payload,
                cargando: false
             }
        case CARGANDO:
            return { ...state, cargando: true }
        default: {
            return state
        }
    }
}