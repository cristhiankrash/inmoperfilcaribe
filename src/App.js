import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux'
import * as usuariosActions from './actions/usuariosActions'
import { AutenticacionServicio } from './servicios/autenticacion.servicio';

// importar componentes
import EncabezadoComponente from './componentes/encabezado.componente'
import InicioComponente from './componentes/inicio/inicio.componente';
import DetalleComponente from './componentes/detalle/detalle.componente';
import FormularioComponente from './componentes/formularios/formulario.componente';
import RegistrarUsuarioComponente from './componentes/usuarios/registrar.component';
import LoginComponente from './componentes/usuarios/login.componente';

class App extends Component {

  constructor() {
    super()
    this.loguearse = this.loguearse.bind(this)
    this.state = {
      usuarioActivo: false
    }
  }

  componentDidMount() {
    this.validarUsuario()
  }

  validarUsuario() {
    const autenticacionServicio = new AutenticacionServicio()
    const usuario = autenticacionServicio.validarUsuario()
    usuario ? this.setState({
      usuarioActivo: true
    }) : this.setState({
      usuarioActivo: false
    })
  }

  loguearse(estadoLogueo) {
    this.props.verificarUsuario()
    this.setState({ usuarioActivo: estadoLogueo })
  }

  render() {
    return (
      <BrowserRouter>
        <div className="encabezado">
          <EncabezadoComponente usuarioActivo={this.state.usuarioActivo} desloguearse={this.loguearse} />
        </div>
        <Switch>
          <Route exact path="/" component={InicioComponente} />
          <Route exact path="/detalle" component={DetalleComponente} />
          <Route exact path='/busqueda' component={DetalleComponente} />
          <Route exact path='/contactanos' component={DetalleComponente} />
          <Route exact path='/nuevo' component={DetalleComponente} />
          <Route exact path='/agregar' component={FormularioComponente} />
          <Route exact path="/formularios" component={FormularioComponente} />
          <Route exact path='/nuevo/usuario' component={RegistrarUsuarioComponente} />
          <Route exact path='/login' render={() => <LoginComponente loguearse={this.loguearse} />} />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (reducers) => {
  return reducers.usuarioReduce;
}

export default connect(mapStateToProps, usuariosActions)(App);
